var express = require('express');
var router = express.Router();
var passport = require('passport');
var InstagramStrategy = require('passport-instagram');

var Promise = require('bluebird');
var request = Promise.promisifyAll(require('request'));

var searchUrl = 'https://api.instagram.com/v1/users/search'

passport.use(new InstagramStrategy({
    clientID: 'db7c025443f94d9abb97bb055c5b6192',
    clientSecret: 'f3a8a29e559d423cb17476b2b7811770',
    callbackURL: "http://127.0.0.1:4000/oauth"
  },
  function(accessToken, refreshToken, profile, done) {
    profile.accessToken = accessToken;
    return done(null, profile);
  }
));

passport.serializeUser(function(user, done) {
  // console.log(user);
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});





/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express', user: req.user });
});


router.get('/oauth',
  passport.authenticate('instagram'),
  function(req, res, next) {
  res.redirect('feed');
});

router.get('/feed', ensureAuthenticated, function(req, res){
  // console.log(req);
  var feedUrl = 'https://api.instagram.com/v1/users/self/feed';
  var params = { access_token: req.user.accessToken };

  request.get({ url: feedUrl, qs: params, json: true }, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      // res.send(body.data);
      console.log(body.data[0]);
      res.render('feed', { posts: body.data });
    }
  });
});


router.get('/search', ensureAuthenticated, function(req, res) {
  var params = {
    access_token: req.user.accessToken,
    q: req.query.q
  };
  request.get({ url: searchUrl, qs: params, json: true }, function(err, response, body) {
console.log(body.data)
   if (!err)
      // res.send(body);
    res.render('search', { users: body.data })
  });
});


function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated())
    return next();
  res.redirect('/oauth');
}


module.exports = router;
