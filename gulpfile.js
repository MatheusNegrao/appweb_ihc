var gulp = require('gulp');

var concat = require('gulp-concat');
 
gulp.task('scripts', function() {
  return gulp.src(
  	'./bower_components/jquery/dist/jquery.js'
  	)
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./public/javascripts/'));
});

gulp.task('styles', function() {
  return gulp.src(
  	'./bower_components/bootstrap/dist/css/bootstrap.css'
  	)
    .pipe(concat('all.css'))
    .pipe(gulp.dest('./public/stylesheets/'));
});